# Introduction

* **Bucket Lists App** is a Flask Powered Bucket Lists App. Bucket List app helps users record things they want to achieve or experience before reaching a certain age.
* Click [here](https://my-bucket-lists.herokuapp.com/) to see a working version.

# Features
  * User should be able to signup and login
  * User should be able to make a Bucket List.
  * User should be able to add tasks to a bucketlist.
  * User should be able to edit and delete bucketlists.
  * User should be able to edit and delete bucketlists items.

# Technologies used
  * Python - Flask
  * Postgre sql
  * Bootstrap